﻿using Eiss.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Models.Data
{
    public class PageDM : IEntityBase, IEntityQuery<PageDM>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Header { get; set; }
        public DateTime DateCreated { get; set; }

        public string SelectQuery(int siteId)
        {
            throw new NotImplementedException();
        }

        public string SelectQuery(int siteId, int id)
        {
            throw new NotImplementedException();
        }

        public string InsertQuery(int siteId, PageDM entity)
        {
            throw new NotImplementedException();
        }

        public string UpdateQuery(int siteId, PageDM entity)
        {
            throw new NotImplementedException();
        }

        public string UpsertQuery(int siteId, PageDM entity)
        {
            throw new NotImplementedException();
        }

        public string DeleteQuery(int siteId, int id)
        {
            throw new NotImplementedException();
        }
    }
}
