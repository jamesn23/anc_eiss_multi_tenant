﻿(function (app) {
    'use strict';

    app.directive('sideBarPortal', sideBarPortal);

    function sideBarPortal() {
        //load  the layout
        return {
            restrict: 'E',
            replace: true,
            //based on the page we need to return the correct one
            templateUrl: '/scripts/spa/layout/sideBarPortal.html'
        }
    }

})(angular.module('common.ui'));