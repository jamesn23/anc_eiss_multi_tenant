﻿using Eiss.Data;
using Eiss.Data.Msss;
using Eiss.Models.Data;
using Eiss.Tenant;
using Eiss.Web.Services.Infrastructure.Account;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace Eiss.Web.Services.Provider
{
    public class AppOAuth : OAuthAuthorizationServerProvider
    {
        private readonly string _publicClientId;

        //get current tenant
        private ITenant _tenant => HttpContext.Current.GetOwinContext().GetCurrentTenant();

        private IAccountService account;

        public AppOAuth(string publicClientId)
        {
            _publicClientId = publicClientId ?? throw new ArgumentNullException("publicClientId");
            this.account = new AccountService();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            //get user
            IUserRepository<UserDM> repository = new UserRepository();
            UserDM user = repository.Select(_tenant.Id, context.UserName);

            //validate credentials
            if (account.ValidateUser(user.Email, context.Password, repository) == null)
            {
                context.SetError("invalid_grant", "The email or password is incorrect.");
                return;
            }

            //create identies
            ClaimsIdentity oAuthIdentity = await CreateIdentityAsync(user, OAuthDefaults.AuthenticationType);
            ClaimsIdentity cookiesIdentity = await CreateIdentityAsync(user, CookieAuthenticationDefaults.AuthenticationType);
            //create ticket and validate
            AuthenticationProperties properties = CreateProperties(user.Email);
            AuthenticationTicket ticket = new AuthenticationTicket(oAuthIdentity, properties);
            context.Validated(ticket);
            context.Request.Context.Authentication.SignIn(cookiesIdentity);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            // Resource owner password credentials does not provide a client ID.
            if (context.ClientId == null)
            {
                context.Validated();
            }

            return Task.FromResult<object>(null);
        }

        public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        {
            if (context.ClientId == _publicClientId)
            {
                Uri expectedRootUri = new Uri(context.Request.Uri, "/");

                if (expectedRootUri.AbsoluteUri == context.RedirectUri)
                {
                    context.Validated();
                }
            }

            return Task.FromResult<object>(null);
        }

        public static AuthenticationProperties CreateProperties(string userName)
        {
            //is this enough?
            IDictionary<string, string> data = new Dictionary<string, string>
            {
                { "userName", userName }
            };
            return new AuthenticationProperties(data);
        }

        //should we move to service ?
        private async Task<ClaimsIdentity> CreateIdentityAsync(UserDM user, string authType)
        {
            //create claims for user
            var claims = new Claim[]{
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name,  user.LastName + ", " + user.FirstName),
                //new Claim(ClaimTypes.Role,  user.RoleId.ToString()),//placeholder get fixed
                new Claim("urn:Custom:SitePermission",  "Read"),//placeholder get fixed
                // Add claim for tenant name
                new Claim("tenantName", HttpContext.Current.GetOwinContext().GetCurrentTenant().Name)
            };

            return await Task.FromResult(new ClaimsIdentity(claims, authType));
        }
    }
}