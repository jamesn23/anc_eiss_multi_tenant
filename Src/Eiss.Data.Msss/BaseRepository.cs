﻿using Eiss.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Eiss.Data.Msss
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="I">Interface</typeparam>
    /// <typeparam name="ENTY">Entity</typeparam>
    public class BaseRepositoryMsss<ENTY> where ENTY : class, IEntityQuery<ENTY>, new()
    {
        /// <summary>
        /// the db connection
        /// </summary>
        protected IDbConnection anc_eiss_connection;

        /// <summary>
        /// the data model used for the invoking class
        /// </summary>
        protected ENTY Entity = new ENTY();

        /// <summary>
        /// base select all implimentation for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public virtual IEnumerable<ENTY> Select(int siteId)
        {
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                var entity = anc_eiss_connection.Query<ENTY>(Entity.SelectQuery(siteId)).ToList();
                return entity;
            }
        }

        /// <summary>
        /// base select single implimentation for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual ENTY Select(int siteId, int id)
        {
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                var entity = anc_eiss_connection.Query<ENTY>(Entity.SelectQuery(siteId, id)).FirstOrDefault();
                return entity;
            }
        }

        /// <summary>
        /// base insert implimentation for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual int Insert(int siteId, ENTY entity)
        {
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                var result = anc_eiss_connection.Execute(Entity.InsertQuery(siteId, entity));
                return result;
            }
        }

        /// <summary>
        /// base update implimentation for current site
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Update(int siteId, ENTY entity)
        {
            bool status = false;
            int result;
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                result = anc_eiss_connection.Execute(Entity.UpdateQuery(siteId, entity));
            }

            if (result >= 1)
            {
                status = true;
            }

            return status;
        }

        /// <summary>
        /// base upsert implimentation for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Upsert(int siteId, ENTY entity)
        {
            bool status = false;
            int result;
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                result = anc_eiss_connection.Execute(Entity.UpsertQuery(siteId, entity));
            }

            if (result >= 1)
            {
                status = true;
            }

            return status;
        }

        /// <summary>
        /// base delete implimentation for current site
        /// </summary>
        /// <param name="id"></param>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public virtual bool Delete(int siteId, int id)
        {
            bool status = false;
            int result;
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                result = anc_eiss_connection.Execute(Entity.DeleteQuery(siteId, id));
            }

            if (result >= 1)
            {
                status = true;
            }

            return status;
        }
    }
}
