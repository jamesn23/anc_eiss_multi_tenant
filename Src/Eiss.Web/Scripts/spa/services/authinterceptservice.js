﻿(function (app) {
    'use strict';

    app.factory('authInterceptService', authInterceptService);

    authInterceptService.$inject = ['$q', '$location', 'localStorageService'];

    function authInterceptService($q, $location, localStorageService) {
        var service = {};

        //should fire before $http sends the request to the API
        function _request(config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        // if there is failure status returned from service
        function _responseError(rejection) {
            if (rejection.status === 401) {
                $location.path('/login');
            }
            return $q.reject(rejection);
        }

        service.request = _request,
        service.responseError = _responseError
        return service;
    }
})(angular.module('common.core'));