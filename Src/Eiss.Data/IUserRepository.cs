﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Data
{
    public interface IUserRepository<T> : IRepository<T>
    {
        /// <summary>
        /// get user for current site
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        T Select(string email);
        /// <summary>
        /// get user for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        T Select(int siteId, string email);

        /// <summary>
        /// Insert known user into current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Insert(int siteId, int userId);
    }
}
