﻿(function (app) {
    'use strict';

    app.directive('sideBarSite', sideBarSite);

    function sideBarSite() {

        //load  the layout
        return {
            restrict: 'E',
            replace: true,
            //based on the page we need to return the correct one
            templateUrl: '/scripts/spa/layout/sideBarSite.html'
        }
    }

})(angular.module('common.ui'));