--/*inital data*/
----add portal and execute
--USE [anc_eiss_portal]
--INSERT INTO [dbo].[tbl_Portal] (Name,DateCreated)
--	VALUES ('EissPortal',GETDATE());

----add 2 sites and execute pass in Id from portal
--USE [anc_eiss_portal]
--INSERT INTO [dbo].[tbl_Site] (PortalId,Name,DateCreated)
--	VALUES (1,'SiteA',GETDATE());

--USE [anc_eiss_portal]
--INSERT INTO [dbo].[tbl_Site] (PortalId,Name,DateCreated)
--	VALUES (1,'SiteB',GETDATE());

----add 2 roles and execute --static  Its reccomended to have 3 at the most no more is needed
--USE [anc_eiss_portal]
--INSERT INTO [dbo].[lu_tbl_Role] (Name)
--	VALUES ('Portal');

--USE [anc_eiss_portal]
--INSERT INTO [dbo].[lu_tbl_Role] (Name)
--	VALUES ('Site');

----add temp master user 
--USE [anc_eiss_portal]
--INSERT INTO [dbo].[tbl_User] (RoleId,FirstName,LastName,Email,[Password],IsApproved,IsLocked,DateCreated,LastLoginDate)
--	VALUES (1,'Nathan','James','NATHANIELTJAMES24@YAHOO.COM','AKSJ2R/YmfJ6h+wQtYh3Vd7lDE6X0bT647wlSV2BE1U+KJs9JpTgIuOZd3Jcn2vydQ==','True','False',GETDATE(),GETDATE());

----add regular user 
--USE [anc_eiss_portal]
--INSERT INTO [dbo].[tbl_User] (RoleId,FirstName,LastName,Email,[Password],IsApproved,IsLocked,DateCreated,LastLoginDate)
--	VALUES (2,'Nata','Jones','NJAMES2@YAHOO.COM','AKSJ2R/YmfJ6h+wQtYh3Vd7lDE6X0bT647wlSV2BE1U+KJs9JpTgIuOZd3Jcn2vydQ==','True','False',GETDATE(),GETDATE());


----add sites to mater user
--USE [anc_eiss_portal]
--INSERT INTO [dbo].[jtbl_User_Sites] (UserId,SiteId)
--	VALUES (2,1);

--USE [anc_eiss_portal]
--INSERT INTO [dbo].[jtbl_User_Sites] (UserId,SiteId)
--	VALUES (2,2);

----add site to regular user
USE [anc_eiss_portal]
INSERT INTO [dbo].[jtbl_User_Sites] (UserId,SiteId)
	VALUES (3,2);

--add temp master user permission
USE [anc_eiss_portal]
INSERT INTO [dbo].[tbl_Permission] (UserId,[Create],[Read],[Update],[Delete],[Grant],[Approve],[DateCreated],[AssignedBy])
	VALUES (1,'True','True','True','True','True','True',GETDATE(),'SQL');


