﻿(function (app) {
    'use strict';

    app.controller('loginCtrl', loginCtrl);

    loginCtrl.$inject = ['$scope', 'accountService', 'notificationService', '$location'];

    function loginCtrl($scope, accountService, notificationService, $location) {
        $scope.login = login;
        $scope.user = {};

        function login() {
            accountService.login($scope.user).then(function (response) {
                $location.path('/');

            }, function (err) {
                notificationService.displayError(err.error_description);
            });
        }
    }
})(angular.module('common.core'));