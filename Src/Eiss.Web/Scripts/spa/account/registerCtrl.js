﻿(function (app) {
    'use strict';

    app.controller('registerCtrl', registerCtrl);

    registerCtrl.$inject = ['$scope', 'accountService', 'notificationService', '$location', '$timeout'];

    function registerCtrl($scope, accountService, notificationService, $location,$timeout) {
        $scope.register = register;
        $scope.importprofile = importprofile;
        $scope.user = {};

        function register() {
            //accountService.register($scope.user, registerCompleted)
            accountService.register($scope.user).then(function (response) {
                notificationService.displaySuccess("User has been registered successfully, you will be redicted to login page.");
                startTimer();
            }, function (response) {
                    //var errors = [];
                    //for (var key in response.data.modelState) {
                    //    for (var i = 0; i < response.data.modelState[key].length; i++) {
                    //        errors.push(response.data.modelState[key][i]);
                    //    }
                    //}
                    notificationService.displayError(response.message);
                });
        }

        function importprofile() {
            //accountService.register($scope.user, registerCompleted)
            accountService.importuser($scope.profile).then(function (response) {
                notificationService.displaySuccess("User has been imported successfully, you will be redicted to login page.");
                startTimer();
            }, function (response) {
                //var errors = [];
                //for (var key in response.data.modelState) {
                //    for (var i = 0; i < response.data.modelState[key].length; i++) {
                //        errors.push(response.data.modelState[key][i]);
                //    }
                //}
                notificationService.displayError(response.message);
            });
        }
        //redirect timer
        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $location.path('/login');
            }, 2000);
        };
    }
})(angular.module('common.core'));