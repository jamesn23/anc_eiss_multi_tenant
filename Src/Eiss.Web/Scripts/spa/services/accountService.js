﻿(function (app) {
    'use strict';

    app.factory('accountService', accountService);

    accountService.$inject = ['notificationService', '$http', '$q', 'localStorageService'];

    function accountService(notificationService, $http, $q, localStorageService) {

        var service = {};
        var _authentication = {
            isAuth: false,
            userName: ""
        };

        function _registration(user) {

            _logOut();

            return $http.post('/api/account/registraton', user).then(function (response) {
                return response;
            });

        }

        function _importuser(user) {

            _logOut();

            return $http.post('/api/account/import', user).then(function (response) {
                return response;
            });

        }


        function _login(user) {
            var data = "grant_type=password&username=" + user.email + "&password=" + user.password;
            var deferred = $q.defer();
            //post data to o-auth validator to reques token
            $http.post('/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
                .success(function (response) {
                    //use local storage
                    localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName });

                    //se authenication
                    _authentication.isAuth = true;
                    _authentication.userName = user.userName;

                    deferred.resolve(response);
                })
                .error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });

            return deferred.promise;
        }

        function _logOut() {
            //remove auth data
            localStorageService.remove('authorizationData');

            _authentication.isAuth = false;
            _authentication.userName = "";
        }

        function _fillAuthData() {
            //add auth data
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                _authentication.isAuth = true;
                _authentication.userName = authData.userName;
            }
        }

        //this allows for accessing local vars for some reason
        service.login = _login;
        service.register = _registration;
        service.logOut = _logOut;
        service.fillAuthData = _fillAuthData;
        service.authentication = _authentication;
        return service;
    }
})(angular.module('common.core'));