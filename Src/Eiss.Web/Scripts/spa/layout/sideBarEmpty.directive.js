﻿(function (app) {
    'use strict';

    app.directive('sideBarEmpty', sideBarEmpty);

    function sideBarEmpty() {
        //load  the layout
        return {
            restrict: 'E',
            replace: true,
            //based on the page we need to return the correct one
            templateUrl:'/scripts/spa/layout/sideBarEmpty.html'
        }
    }

})(angular.module('common.ui'));