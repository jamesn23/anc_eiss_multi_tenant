﻿using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Eiss.Tenant
{
    /// <summary>
    /// Extensions methods
    /// </summary>
    public static class OwinExtension
    {
        /// <summary>
        /// Add TenantCore to the app builder
        /// </summary>
        /// <param name="app">The AppBuilder instance</param>
        /// <param name="resolver">The Tenant Resolver instance</param>
        /// <param name="callback">The callback method</param>
        /// <returns>AppBuilder with TenantCore</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static IAppBuilder UseTenantCore(this IAppBuilder app, ITenantResolver resolver,
            Func<ITenant, bool> callback = null)
        {
            if (app == null)
            {
                throw new ArgumentNullException(nameof(app));
            }

            if (resolver == null)
            {
                throw new ArgumentNullException(nameof(resolver));
            }

            return app.Use(typeof(TenantCoreMiddleware), resolver, callback);
        }

        /// <summary>
        /// Retrieves the current Tenant from a HttpContext
        /// </summary>
        /// <param name="context">The Http context</param>
        /// <returns>The Tenant</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static ITenant GetCurrentTenant(this HttpContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.GetCurrentTenant();
        }

        /// <summary>
        /// Retrieves the current Tenant from a Owin Context
        /// </summary>
        /// <param name="context">The Owin context</param>
        /// <returns>The Tenant</returns>
        /// <exception cref="ArgumentNullException"></exception>
        public static ITenant GetCurrentTenant(this IOwinContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            return context.Get<ITenant>(TenantCoreMiddleware.OwinPropertyName);
        }

        /// <summary>
        /// Retrieves the current user from Tenant
        /// </summary>
        /// <param name="context">The Owin context</param>
        /// <returns>The Tenant</returns>
        /// <exception cref="ArgumentNullException"></exception>
        //public static  UserVM GetCurrentUser(this ITenant tenant)
        //{
        //    if (context == null)
        //    {
        //        throw new ArgumentNullException(nameof(context));
        //    }

        //    return context.Get<ITenant>(TenantCoreMiddleware.OwinPropertyName);
        //}
    }
}

