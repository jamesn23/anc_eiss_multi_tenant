﻿using log4net.Appender;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eiss.Logging
{
    public class LoggerAppender : AppenderSkeleton
    {
        protected override void Append(LoggingEvent loggingEvent)
        {
            ErrorLevel logLevel;
            switch (loggingEvent.Level.Name)
            {
                case "DEBUG":
                    {
                        logLevel = ErrorLevel.DEBUG;
                        break;
                    }
                case "INFO":
                    {
                        logLevel = ErrorLevel.INFO;
                        break;
                    }
                case "WARN":
                    {
                        logLevel = ErrorLevel.WARNING;
                        break;
                    }
                case "ERROR":
                case "FATAL":
                default:
                    {
                        logLevel = ErrorLevel.ERROR;
                        break;
                    }

            }

            string logMessage = RenderLoggingEvent(loggingEvent);

            Logger.Log(logLevel, loggingEvent.LoggerName, logMessage);
        }

        public override void AddFilter(log4net.Filter.IFilter filter)
        {
            base.AddFilter(filter);
        }
    }
}
