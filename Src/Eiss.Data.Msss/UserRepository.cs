﻿using Eiss.Models.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Eiss.Data.Msss
{
    public class UserRepository : BaseRepositoryMsss<UserDM>, IUserRepository<UserDM>
    {
        /// <summary>
        /// search for user by email in system
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserDM Select(string email)
        {
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                var entity = anc_eiss_connection.Query<UserDM>(Entity.SelectQuery(email)).FirstOrDefault();
                return entity;
            }
        }

        /// <summary>
        /// search for user by email in current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public UserDM Select(int siteId, string email)
        {
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                var entity = anc_eiss_connection.Query<UserDM>(Entity.SelectQuery(siteId, email)).FirstOrDefault();
                return entity;
            }
        }

        /// <summary>
        /// Add known user to current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int Insert(int siteId, int userId)
        {
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                var result = anc_eiss_connection.Execute(Entity.InsertQuery(siteId, userId));
                return result;
            }
        }
    }
}
