﻿using Eiss.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using Eiss.Tenant;
using System.Web.Mvc;
using System.Web;

namespace Eiss.Web.Controllers
{
    public class HomeController : Controller
    {
        
        public ActionResult Index()
        {
            var T = System.Web.HttpContext.Current.GetOwinContext().GetCurrentTenant();
            return View(T);
        }
    }
}