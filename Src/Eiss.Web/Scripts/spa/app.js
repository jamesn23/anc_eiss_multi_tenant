﻿(function () {
    'use strict';

    angular.module('eissPortal', ['common.core', 'common.ui', 'kendo.directives', 'LocalStorageModule', 'angular-loading-bar'])
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider'];
    function config($routeProvider, $locationProvider, $httpProvider) {

        //auth interceptor
        $httpProvider.interceptors.push('authInterceptService');

        //routing
        $routeProvider
            .when("/", {
                templateUrl: "scripts/spa/home/index.html",
                controller: "indexCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/login", {
                templateUrl: "scripts/spa/account/login.html",
                controller: "loginCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/register", {
                templateUrl: "scripts/spa/account/register.html",
                controller: "registerCtrl"
            })
            .when("/portal/home", {
                templateUrl: "scripts/spa/portal/portalindex.html",
                controller: "portalindexCtrl",
                resolve: { isAuthenticated: isAuthenticated }
            })
            .when("/error/404", {
                templateUrl: "scripts/spa/error/404.html"
            })
            .otherwise({ redirectTo: "/error/404" }); // Render 404 view

        // use the HTML5 History API
        //$locationProvider.html5Mode(true);
    }

    run.$inject = ['$rootScope', '$location', '$http', 'accountService'];

    function run($rootScope, $location, $http, accountService) {
        //// handle page refreshes
        //$rootScope.repository = $cookieStore.get('repository') || {};
        //if ($rootScope.repository.loggedUser) {
        //    $http.defaults.headers.common['Authorization'] = $rootScope.repository.loggedUser.authdata;
        //}

        //add anti forgery from index to our headers
        $http.defaults.headers.common['X-XSRF-Token'] =
            angular.element('input[name="__RequestVerificationToken"]').attr('value');

        $(document).ready(function () {
            $(".fancybox").fancybox({
                openEffect: 'none',
                closeEffect: 'none'
            });

            $('.fancybox-media').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                helpers: {
                    media: {}
                }
            });

            $('[data-toggle=offcanvas]').click(function () {
                $('.row-offcanvas').toggleClass('active');
            });
        });

        //auth service
        accountService.fillAuthData();
    }

    isAuthenticated.$inject = ['accountService', '$rootScope', '$location'];

    function isAuthenticated(accountService, $rootScope, $location) {
        if (!accountService.authentication.isAuth) {
            $rootScope.previousState = $location.path();
            $location.path('/login');
        }
    }
})();