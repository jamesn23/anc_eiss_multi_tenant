USE [anc_eiss_portal]

SELECT * FROM [dbo].[tbl_Portal]

SELECT * FROM [dbo].[tbl_Site]

SELECT * FROM [dbo].[lu_tbl_Role]

SELECT * FROM [dbo].[tbl_User] usr

SELECT * FROM [dbo].jtbl_User_Sites
SELECT 
	usr.Id as UserId, 
	usr.FirstName, 
	usr.LastName, 
	usr.Email, 
	usr.[Password],
	usr.[PasswordSalt],
	usr.IsApproved,
	usr.IsLocked,
	usr.DateCreated,
	usr.LastLoginDate,
	rle.Id as RoleId,
	rle.Name as RoleName
FROM [dbo].[tbl_User] usr
LEFT JOIN [dbo].[lu_tbl_Role] [rle] ON [rle].Id = usr.RoleId
INNER JOIN [dbo].[jtbl_User_Sites] [usrsite] ON [usrsite].UserId = usr.Id
--LEFT JOIN [dbo].[tbl_Site] [site] ON [site].Id = [usrsite].SiteId
WHERE [usrsite].SiteId=2

SELECT * FROM [dbo].[tbl_Permission]

DECLARE @currId INT;
INSERT INTO [dbo].[tbl_User] (RoleId,FirstName,LastName,Email,[Password],IsApproved,IsLocked,DateCreated,LastLoginDate)
	VALUES (2,'{1}','{2}','{3}','{4}','False','False',GETDATE(),GETDATE())
	SET @currId = SCOPE_IDENTITY()

INSERT INTO [dbo].[jtbl_User_Sites] (UserId,SiteId)
	VALUES (@currId,2);

	Update [dbo].[tbl_Site]
	SET [Theme] = '~/content/css/theme/bootstrap.cerulean.css'
	Where id = 1

	Update [dbo].[tbl_Site]
	SET [Theme] = '~/content/css/theme/bootstrap.yeti.css'
	Where id = 2

	Update [dbo].[tbl_Site]
	SET [Theme] = '~/content/css/theme/bootstrap.darkly.css'
	Where id = 3

	INSERT INTO [dbo].[tbl_Site] (PortalId,Name,DateCreated,HostUrl)
	 VALUES (1,'Admin',GETDATE(),'localhost:6000');

	 DELETE TOP(1) FROM [dbo].[jtbl_User_Sites]
	 WHERE UserId =2

	 Update [dbo].[jtbl_User_Sites]
	SET SiteId= 3
	Where UserId = 2


	SELECT Id,Name,DateCreated,HostUrl FROM [dbo].[tbl_Site]