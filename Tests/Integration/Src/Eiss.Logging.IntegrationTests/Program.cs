﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Globalization;

namespace Eiss.Logging.IntegrationTests
{
    class Program
    {
        static void Main(string[] args)
        {
            //Logging.Logger
            XmlDocument loggerConfigXml = new XmlDocument();
            loggerConfigXml.LoadXml(Logger.LoggerAppenderXml);
            Logger.CreateLogger("Logging.Logger.Test.log", "Eiss.Logger", null, loggerConfigXml);

            //Get loggers
            var loggers = LogManager.GetCurrentLoggers();

            //Log1
            Logger.Log(ErrorLevel.ERROR, "Type string", "Message here!");
        }
    }
}
