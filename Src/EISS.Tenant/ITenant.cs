﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Tenant
{
    /// <summary>
    /// Public interface to create Tenant/Site instance
    /// </summary>
    public interface ITenant
    {
        /// <summary>
        /// Tenant's Id
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Tenant's name
        /// </summary>
        string Name { get; }

        /// <summary>
        /// The Tenant domains
        /// </summary>
        string HostUrl { get; }

        /// <summary>
        /// The Tenant Theme
        /// </summary>
        string Theme { get; }

        /// <summary>
        /// Properties collection maybe we can stor site info like users pages etc.. need to look into that info
        /// </summary>
        Dictionary<string, List<object>> Properties { get; }

        /// <summary>
        /// The Resolver instance
        /// </summary>
        ITenantResolver Resolver { get; set; }
    }
}
