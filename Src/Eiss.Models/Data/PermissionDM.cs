﻿using Eiss.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Models.Data
{
    public class PermissionDM : IEntityBase, IEntityQuery<PermissionDM>
    {
        public int Id { get; set; }
        public string SiteId { get; set; }
        public string PageId { get; set; }
        public string UserId { get; set; }
        public bool hasCreate { get; set; }
        public bool hasRead { get; set; }
        public bool hasUpdate { get; set; }
        public bool hasDelete { get; set; }
        public bool hasGrant { get; set; }
        public bool hasApprove { get; set; }
        public DateTime DateCreated { get; set; }
        public string AssignedBy { get; set; }

        public string SelectQuery(int siteId)
        {
            throw new NotImplementedException();
        }

        public string SelectQuery(int siteId, int id)
        {
            throw new NotImplementedException();
        }

        public string InsertQuery(int siteId, PermissionDM entity)
        {
            throw new NotImplementedException();
        }

        public string UpdateQuery(int siteId, PermissionDM entity)
        {
            throw new NotImplementedException();
        }

        public string UpsertQuery(int siteId, PermissionDM entity)
        {
            throw new NotImplementedException();
        }

        public string DeleteQuery(int siteId, int id)
        {
            throw new NotImplementedException();
        }
    }
}
