﻿using Autofac;
using Autofac.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using Eiss.Data;
using Eiss.Data.Msss;
using System.Net.Http.Headers;
using Eiss.Web.Services.Infrastructure.Account;

namespace Eiss.Web.App_Start
{
        public class AutofacWebapiConfig
        {
            public static IContainer Container;

            public static void Initialize(HttpConfiguration config)
            {
                Initialize(config, RegisterServices(new ContainerBuilder()));
            }


            public static void Initialize(HttpConfiguration config, IContainer container)
            {
                config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
                
            }

            private static IContainer RegisterServices(ContainerBuilder builder)
            {
                //Register your Web API controllers.  
                builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

                builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
                    .AsImplementedInterfaces()
                    .InstancePerLifetimeScope();

            //builder.RegisterType<PersonRepository>().As<IPersonRepository<>>().InstancePerRequest();
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerRequest();

            //Set the dependency resolver to be Autofac.  
            Container = builder.Build();

                return Container;
            }
        }  
    }