﻿using Eiss.Data;
using Eiss.Models.Data;
using Eiss.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Web.Services.Infrastructure.Account
{
    public interface IAccountService
    {
        string CreateSalt();

        string EncryptPassword(string password, string salt);

        bool IsPasswordValid(UserVM user, string password);

        bool IsUserValid(UserVM user, string password);

        UserVM ValidateUser(string email, string password, IUserRepository<UserDM> repository);

        UserVM AddExistingUser(UserVM entity, IUserRepository<UserDM> repository, int tenantId);

        UserVM CreateUser(UserVM entity, IUserRepository<UserDM> repository, int tenantId);
    }
}
