﻿using AutoMapper;
using Eiss.Data;
using Eiss.Models.Data;
using Eiss.Models.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Web.Services.Infrastructure.Account
{
    public class AccountService : IAccountService
    {
        public string CreateSalt()
        {
            var data = new byte[0x10];
            using (var cryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                cryptoServiceProvider.GetBytes(data);
                return Convert.ToBase64String(data);
            }
        }

        public string EncryptPassword(string password, string salt)
        {
            using (var sha256 = SHA256.Create())
            {
                var saltedPassword = string.Format("{0}{1}", salt, password);
                byte[] saltedPasswordAsBytes = Encoding.UTF8.GetBytes(saltedPassword);
                return Convert.ToBase64String(sha256.ComputeHash(saltedPasswordAsBytes));
            }
        }

        
        public bool IsPasswordValid(UserVM user, string password)
        {
            string _password = EncryptPassword(password, user.Salt);
            return string.Equals(_password, user.Password);
        }

        public bool IsUserValid(UserVM user, string password)
        {
            if (IsPasswordValid(user, password))
            {
                return true;
            }
            return false;
        }

        public UserVM ValidateUser(string email, string password, IUserRepository<UserDM> repository)
        {
            var user = Mapper.Map<UserDM, UserVM>(repository.Select(email));

            return (user != null && IsUserValid(user, password)) ? user : null;
        }

        public UserVM AddExistingUser(UserVM entity, IUserRepository<UserDM> repository,int tenantId)
        {
            if (entity != null) { throw new Exception("user not available"); }

            var result = repository.Insert(tenantId, entity.Id); ///USE AUTOMAPPER HERE!!!

            //verify no issues on insert
            if (result < 1)
            {
                return entity;
            }
            else
            {
                return null;
            }
        }

        public UserVM CreateUser(UserVM entity, IUserRepository<UserDM> repository, int tenantId)
        {
            //set user defaults
            entity.Salt = CreateSalt();
            entity.Password = EncryptPassword(entity.Password, entity.Salt);
            var result = repository.Insert(tenantId, Mapper.Map<UserVM, UserDM>(entity)); ///USE AUTOMAPPER HERE!!!

            //verify no issues on insert
            if (result < 1)
            {
                return entity;
            }
            else
            {
                return null;
            }
        }
    }
}
