﻿(function (app) {
    'use strict';

    app.controller('rootCtrl', rootCtrl);

    rootCtrl.$inject = ['$scope', '$location', 'accountService', '$rootScope'];
    function rootCtrl($scope, $location, accountService, $rootScope) {

        $scope.userData = {};

        $scope.userData.displayUserInfo = displayUserInfo;
        $scope.logout = logout;

        //helps us get the correct side navin layout
        $scope.navtype = function () {
            var location = $location.path()
            if (location.toLowerCase().indexOf("/portal") > -1) { return "portal" }

            else if (location.toLowerCase().indexOf("/login") > -1 ||
                location.toLowerCase().indexOf("/register") > -1 ||
                location.toLowerCase().indexOf("/error") > -1) {
                return "empty";
            }

            else { return "site"; }
        }


        function displayUserInfo() {
            $scope.userData.isAuth = accountService.authentication.isAuth;

            if ($scope.userData.isAuth) {
                $scope.username = accountService.authentication.userName;
            }
        }

        function logout() {
            accountService.logOut;
        }

        $scope.userData.displayUserInfo();
    }

})(angular.module('eissPortal'));
