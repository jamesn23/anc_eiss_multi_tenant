use [master]
drop database [anc_eiss_portal]

create database [anc_eiss_portal]

use [anc_eiss_portal]
	
	CREATE TABLE [dbo].[tbl_Portal](
	[Id] [int] PRIMARY KEY IDENTITY NOT NULL,
	[Name] nvarchar(100) not null,
	[DateCreated] datetime not null
	)

	CREATE TABLE [dbo].[tbl_Site](
	[Id] [int] PRIMARY KEY IDENTITY NOT NULL,
	[PortalId] [int] FOREIGN KEY REFERENCES tbl_Portal(Id) NOT NULL,
	[Name] nvarchar(100) not null,
	[Title] nvarchar(100) not null,
	[HostUrl] nvarchar(Max) not null,
	[Theme] nvarchar(100) not null,
	[DateCreated] datetime not null,
	)

	CREATE TABLE [dbo].[tbl_Page](
	[Id] [int] PRIMARY KEY IDENTITY NOT NULL,
	[SiteId] [int] FOREIGN KEY REFERENCES tbl_Site(Id) NOT NULL,
	[Name] nvarchar(100) not null,
	[DateCreated] datetime not null
	)

	CREATE TABLE [dbo].[lu_tbl_Role](
	[Id] [int] PRIMARY KEY IDENTITY NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	)

	CREATE TABLE [dbo].[tbl_User](
	[Id] [int] PRIMARY KEY IDENTITY NOT NULL,
	[RoleId] [int] FOREIGN KEY REFERENCES lu_tbl_Role(Id) NOT NULL,
	[FirstName] nvarchar(100) not null,
	[LastName] nvarchar(100) not null,
	[Email] nvarchar(100) not null,
	[Password] nvarchar(max) not null,
	[PasswordSalt] nvarchar(max) not null,
	[IsApproved] nvarchar(5) not null,
	[IsLocked] nvarchar(5) not null,
	[DateCreated] datetime not null,
	[LastLoginDate] datetime not null
	)
	
	CREATE TABLE [dbo].[jtbl_User_Sites](--junction table
	--[Id] [int] PRIMARY KEY IDENTITY NOT NULL
	[UserId] [int] FOREIGN KEY REFERENCES tbl_User(Id) NOT NULL,
	[SiteId] [int] FOREIGN KEY REFERENCES tbl_Site(Id) NULL,
	
	)

	CREATE TABLE [dbo].[tbl_Permission](
	[Id] [int] PRIMARY KEY IDENTITY NOT NULL,
	[SiteId] [int] FOREIGN KEY REFERENCES tbl_Site(Id) NULL,
	[PageId] [int] FOREIGN KEY REFERENCES tbl_Page(Id) NULL,
	[UserId] [int] FOREIGN KEY REFERENCES tbl_User(Id) NOT NULL,
	[Create] nvarchar(5) not null,
	[Read] nvarchar(5) not null,
	[Update] nvarchar(5) not null,
	[Delete] nvarchar(5) not null,
	[Grant] nvarchar(5) not null,
	[Approve] nvarchar(5) not null,
	[DateCreated] datetime not null,
	[AssignedBy] nvarchar(max) not null
	)

	--may not need
	--CREATE TABLE [dbo].[tbl_Claim](
	--[Id] [bigint] PRIMARY KEY IDENTITY NOT NULL,
	--[SiteId] [int] FOREIGN KEY REFERENCES tbl_Site(Id) NULL,
	--[UserId] [int] FOREIGN KEY REFERENCES tbl_User(Id) NOT NULL,
	--[ClaimSubject] [nvarchar](max) NULL,
	--[ClaimType] [nvarchar](max) NULL,
	--[ClaimValue] [nvarchar](max) NULL,
	--[CreatedDate] [datetime] NOT NULL
	--)