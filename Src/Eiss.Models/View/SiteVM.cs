﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Models.View
{
    public class SiteVM
    {
        public long? Id { get; set; }
        //public PortalDM Portal { get; set; }
        public string Name { get; set; }
        public string HostUrl { get; set; }
        
        public string Theme { get; set; }
        public DateTime DateCreated { get; set; }
        //public IEnumerable<UserDM> Users { get; set; }
        //public IEnumerable<PageDM> Pages { get; set; }
    }
}
