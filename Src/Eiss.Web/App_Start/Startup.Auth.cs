﻿using Eiss.Data;
using Eiss.Data.Msss;
using Eiss.Models.Data;
using Eiss.Tenant;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eiss.Web.Services.Provider;
using Eiss.Web.Services.Api;
using Eiss.Web.Services.Infrastructure.Account;

namespace Eiss.Web
{
    public partial class Startup
    {
        //not sure if I wanted to call db from here maybe there is a better way?
        private ISiteRepository<SiteDM> repository = new SiteRepository();

        public static OAuthAuthorizationServerOptions OAuthOptions { get; private set; }

        public static string PublicClientId { get; private set; }

        public void ConfigureAuth(IAppBuilder app)
        {
            //add ref to cors may need to test to ensure we are up
            app.UseCors(CorsOptions.AllowAll);

            // SetUp Multitenancy with TenantCore proj
            var tenants = new List<ITenant>();

            //for each site map back as tenant
            repository.Select().ToList().ForEach(t => tenants.Add(new Eiss.Tenant.Tenant(t.Id, t.Name, t.HostUrl, t.Theme)));

            app.UseTenantCore(new HostNameResolver(tenants));

            //I think we need this but we should be using bearer tokens so idk
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/logon"),
                CookieName = "SecureAuth",
                CookieSecure = CookieSecureOption.SameAsRequest,
                CookieHttpOnly = true,
                Provider = new CookieAuthenticationProvider
                {
                    OnApplyRedirect = ctx =>
                    {
                        if (!IsWebApiRequest(ctx.Request))
                        {
                            ctx.Response.Redirect(ctx.RedirectUri);
                        }
                    }
                },
                ExpireTimeSpan = TimeSpan.FromDays(14),
                SlidingExpiration = true
            });
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            // Configure the application for OAuth based flow
            PublicClientId = "self";
            OAuthOptions = new OAuthAuthorizationServerOptions
            {
                TokenEndpointPath = new PathString("/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = new AppOAuth(PublicClientId),
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthBearerAuthentication(AccountController.OAuthBearerOptions);
        }

        private static bool IsWebApiRequest(IOwinRequest request)
        {
            return (request.Path.StartsWithSegments(new PathString("/api")));
        }
    }
}