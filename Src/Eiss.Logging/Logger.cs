﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Microsoft.Win32.SafeHandles;
using System.Configuration;

namespace Eiss.Logging
{
    /// <summary>
    /// Provide a means to turn off existing Log statements for the purpose of profiling code without the Log noise.
    /// Since enums are compiled, the Log method evaluates whether any (int)ErrorLevel.yadda equals BaseErrorLevel.OFF,
    /// if so, return out of the Log method without logging anything.
    /// This requires that you reassign one or more ErrorLevels to BaseErrorLevel.OFF then recompile.
    /// Usefull although nasty...(make sure to reset before release).
    /// </summary>
    public enum BaseErrorLevel
    {
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        FATAL,
        ALL,
        OFF
    }

    public enum ErrorLevel
    {
        DEBUG = BaseErrorLevel.DEBUG,
        INFO = BaseErrorLevel.INFO,
        WARNING = BaseErrorLevel.WARNING,
        ERROR = BaseErrorLevel.ERROR
    }
    
    public static class Logger
    {
        public static readonly string LoggerAppenderXml = @"
            <log4net>
             <appender name='ConsoleAppender' type='log4net.Appender.ConsoleAppender,log4net'>
              <layout type='log4net.Layout.PatternLayout,log4net'>
               <conversionPattern value='%date %7level %message%newline%exception' />
              </layout>
              <filter type='log4net.Filter.LevelRangeFilter'>
               <levelMin value='DEBUG' />
               <levelMax value='FATAL' />
              </filter>
              <filter type='log4net.Filter.DenyAllFilter' />
             </appender>
            <appender name='EventLogAppender' type='log4net.Appender.EventLogAppender,log4net' >
                <applicationName value='MyApp' />
                <layout type = 'log4net.Layout.PatternLayout' >
                   <conversionPattern value='%date [%thread] %-5level %logger [%property{NDC}] - %message%newline' />
                </layout>
            </appender>
             <appender name='FileAppender' type='log4net.Appender.FileAppender'>
              <file value='C:\Users\mbeidler\source\log-file.txt' />
              <appendToFile value='true' />
              <layout type='log4net.Layout.PatternLayout,log4net'>
               <conversionPattern value='%date %7level %message%newline%exception' />
              </layout>
             </appender>
             <appender name='OutputDebugStringAppender' type='log4net.Appender.OutputDebugStringAppender,log4net'>
              <layout type='log4net.Layout.PatternLayout,log4net'>
               <conversionPattern value='%date %7level %message%newline%exception' />
              </layout>
              <filter type='log4net.Filter.LevelRangeFilter'>
               <levelMin value='DEBUG' />
               <levelMax value='FATAL' />
              </filter>
              <filter type='log4net.Filter.DenyAllFilter' />
             </appender>
             <root>
              <level value='ALL' />
              <appender-ref ref='ConsoleAppender' />
              <appender-ref ref='FileAppender' />
              <appender-ref ref='OutputDebugStringAppender' />
             </root>
             <logger name='CaseAdminstrationTool.Logger' additivity='false'>
              <level value='ALL' />
              <appender-ref ref='ConsoleAppender' />
             </logger>
            </log4net>
            ";
        public static string fileNamePrefix;
        public static string fileLocation = null;
        private static string dateFormatString;
        private static string normalLineFormatString;
        private static string serviceLineFormatString;
        private static string excFormatString;
        private static TraceListener listener;  // parent class to both TextWriterTraceListener & the custom Log4netTraceListener 
        private static string currentExeName;
        private static Version currentExeVersion;
        public static  log4net.ILog instance { get; private set; }

        public class Log4netTraceListener : System.Diagnostics.TraceListener
        {
            private readonly log4net.ILog _log;

            public Log4netTraceListener()
            {
                _log = log4net.LogManager.GetLogger("System.Diagnostics.Redirection");
            }

            public Log4netTraceListener(log4net.ILog log)
            {
                _log = log;
            }

            public Log4netTraceListener(StreamWriter stream)/* : base(stream)*/
            {
                _log = log4net.LogManager.GetLogger("System.Diagnostics.Redirection");
            }

            public override void Write(string message)
            {
                if (_log != null) _log.Debug(message);
            }

            public override void WriteLine(string message)
            {
                if (_log != null) _log.Debug(message);
            }
        }

        /// <summary>
        /// Creates the logger.
        /// The optional paramater is added to permit legacy applications to function as expected with new applications pioneering
        /// a more configurable logging scheme available within log4net.
        /// </summary>
        /// <param name="logName">The path to the logfile that lives as a log4net global property used in the config parameter.</param>
        /// <param name="exeName">Name of the executable.</param>
        /// <param name="exeVersion">The executable version.</param>
        /// <param name="config">The configuration document containing the xml initialization values for log4net
        /// (which has been typed to object to alleviate the need to include 'using System.Xml' in every file that uses this function.)</param>
        public static void CreateLogger(string logName, string exeName, Version exeVersion, object config = null)
        {
            //FNPADToolkit.ADInitializing.EnsureLocalAppData();
            if (fileLocation == null)
            {
                fileLocation = Environment.GetEnvironmentVariable("TEMP");
            }
            fileNamePrefix = DateTime.Now.ToString("yyyyMMddHHmmssffff_");
            dateFormatString = "yyyy-MM-dd HH:mm:ss.ffff";
            serviceLineFormatString = "{0," + dateFormatString.Length + "} {1,7} {2,16} {3,32} {4}";
            normalLineFormatString = "{0," + dateFormatString.Length + "} {1,7} {2,16} {3}{4}";
            excFormatString = "{0}: {1}\r\n{2}";

            //Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);

            if (config != null && config is XmlDocument)
            {
                ConfigureLog4net(logName, config as XmlDocument);

                string configPath = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None).FilePath;
                var configXml = new XmlDocument();
                configXml.Load(configPath);
                XmlNode node = configXml.SelectSingleNode("//system.diagnostics//sources//source//listeners");
                if (node != null)
                {
                    var elem = node.SelectSingleNode("//add[@initializeData='Trace.svclog']") as XmlElement;
                    if (elem != null)
                    {
                        elem.Attributes["initializeData"].Value = Path.Combine(fileLocation, fileNamePrefix + "CAT.svclog");
                        configXml.Save(configPath);
                        Trace.Refresh();
                    }
                }

                listener = new Log4netTraceListener(/*File.AppendText(Path.Combine(fileLocation, fileNamePrefix + "Trace.svclog"))*/);
            }
            else
            {
                listener = new TextWriterTraceListener(File.AppendText(Path.Combine(fileLocation, fileNamePrefix + logName)));

                //setup log4net
                string layoutString = "%message%newline%exception";
                log4net.Layout.PatternLayout patternLayout = new log4net.Layout.PatternLayout();
                patternLayout.ConversionPattern = layoutString;
                patternLayout.ActivateOptions();

                var loggerAppender = new LoggerAppender();
                loggerAppender.Layout = patternLayout;
                loggerAppender.ActivateOptions();
                log4net.Config.BasicConfigurator.Configure(loggerAppender);
            }

            Trace.AutoFlush = false;
            Trace.Listeners.Add(listener);
            Trace.TraceInformation("Added a log4net trace listener.");
            Trace.Flush();

            currentExeName = exeName;
            currentExeVersion = exeVersion;

            Logger.Log(ErrorLevel.INFO, "Logger", string.Format("{0} v{1}", currentExeName, currentExeVersion));
        }

        public static void RaiseConfigurationChanged(log4net.Core.Level level)
        {
            var hier = log4net.LogManager.GetRepository() as log4net.Repository.Hierarchy.Hierarchy;
            hier.Root.Level = level;
            var logger = log4net.LogManager.GetLogger(typeof(Logger));
            (logger.Logger as log4net.Repository.Hierarchy.Logger).Level = level;
            hier.RaiseConfigurationChanged(EventArgs.Empty);
        }

        private static readonly string defaultPropertyName = "loggerName";
        private static void ConfigureLog4net(string logName, XmlDocument config)
        {
            string loggerPath = Path.Combine(fileLocation, fileNamePrefix + logName);
            log4net.GlobalContext.Properties[defaultPropertyName] = loggerPath;

            XmlNodeList elemList = null;
            try
            {
                elemList = config.SelectNodes("/log4net/appender/file/conversionPattern");
            }
            catch { }
            if (elemList != null && elemList.Count > 0)
            {
                List<string[]> map = new List<string[]>();
                foreach (XmlNode item in elemList)
                {
                    if (item.ParentNode.ParentNode.Name.Equals("appender"))
                    {
                        System.Xml.XmlNode node = item.ParentNode.ParentNode;
                        var name = node.Attributes.GetNamedItem("name").Value;
                        var property = item.Attributes.GetNamedItem("value").Value;
                        if (property.Contains("%property"))
                        {
                            property = Regex.Replace(property, "(^.*[{])|([}].*$)", "");
                            map.Add(new string[] { name, property });
                        }
                    }
                }
                foreach (string[] item in map)
                {
                    string[] pathArray = Regex.Split(loggerPath, "[.]");
                    string nextLoggerPath = pathArray[0] + "_" + item[1] + ".log";
                    log4net.Util.GlobalContextProperties test = log4net.GlobalContext.Properties;
                    if (test[item[1]] == null)
                    {
                        log4net.GlobalContext.Properties[item[1]] = nextLoggerPath;
                    }
                }
            }
            log4net.Config.XmlConfigurator.Configure((config as XmlDocument).DocumentElement);
        }

        public static void CleanupLogger()
        {
            if (listener != null)
            {
                listener.Flush();
                listener.Close();
            }
        }

        //public static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        //{
        //    Log("Logger", "Fatal error:", e.Exception);
        //    MessageBox.Show("A fatal error has occurred:\n\n" + e.Exception.Message, "Fatal error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //    Trace.Flush();
        //    Environment.Exit(1);
        //}

        public static string GetExceptionString(Exception e)
        {
            if (e.InnerException == null)
            {
                return string.Format(excFormatString, e.GetType().Name, e.Message, e.StackTrace);
            }
            else
            {
                return string.Format(excFormatString, e.GetType().Name, e.Message, e.StackTrace) + "\r\nInner Exception:\r\n" + GetExceptionString(e.InnerException);
            }
        }

        public static void xLog(ErrorLevel errorLevel, string message,
                                [System.Runtime.CompilerServices.CallerFilePath] string srcPath = "",
                                [System.Runtime.CompilerServices.CallerMemberName] string srcFunc = "",
                                [System.Runtime.CompilerServices.CallerLineNumber] int srcLine = 0)
        {
            string typeString = Path.GetFileNameWithoutExtension(srcPath) + "\\" + srcFunc + "'" + srcLine + ": ";
#if LEGACY_LOG
            Log(errorLevel, typeString, message);
#else
            realLog(errorLevel, typeString, message);
#endif
        }

        public static void Log(ErrorLevel errorLevel, string senderType, string message,
                               [System.Runtime.CompilerServices.CallerFilePath] string srcPath = "",
                               [System.Runtime.CompilerServices.CallerMemberName] string srcFunc = "",
                               [System.Runtime.CompilerServices.CallerLineNumber] int srcLine = 0)
        {
            if ((int)errorLevel == (int)BaseErrorLevel.OFF) return;
            string typeString = BuildTypeString(srcPath, srcFunc, srcLine);
            string msg = BuildMessage(senderType, message);
#if LEGACY_LOG
            Log(errorLevel, typeString, msg);
#else
            if (listener is TextWriterTraceListener)
                realLog(errorLevel, typeString, msg);
            else
                Log(errorLevel, (object)typeString, msg);
#endif
        }

        private static string BuildMessage(string senderType, string message)
        {
            string typeSender = senderType.Substring(senderType.Length > 16 ? senderType.Length - 16 : 0);
            typeSender = string.Format("{0,-16}", typeSender);
            message = string.Format("{0}: {1}", typeSender, message == null ? "" : message);
            return message;
        }

        private static string BuildTypeString(string srcPath, string srcFunc, int srcLine)
        {
            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(srcPath);
            string typeString = string.Format(@"{0}\{1}'{2}", fileNameWithoutExtension, srcFunc, srcLine);
            if (typeString.Length > 40) // remove non-leading lowercase vowels for filename
            {
                Regex pattern = new Regex(@"[aeiou]");
                fileNameWithoutExtension = pattern.Replace(fileNameWithoutExtension, "", -1, 1);
                typeString = string.Format(@"{0}\{1}'{2}", fileNameWithoutExtension, srcFunc, srcLine);
            }
            typeString = typeString.Substring(typeString.Length > 40 ? typeString.Length - 40 : 0);
            typeString = string.Format("{0,-40}:", typeString);
            return typeString;
        }

        public static void Log(log4net.Core.Level level, object message, Exception exception)
        {
            if (Logger.instance == null)
            {
                Logger.instance = log4net.LogManager.GetLogger(typeof(Logger));
            }
            switch(level.Value.ToString())
            {
                case "INFO":
                    Logger.instance.Info(message, exception);
                    break;
                case "DEBUG":
                    Logger.instance.Debug(message, exception);
                    break;
                case "WARNING":
                    Logger.instance.Warn(message, exception);
                    break;
                case "ERROR":
                    Logger.instance.Error(message, exception);
                    break;
                default:
                    Logger.instance.Warn(message, exception);
                    break;
            }
        }

        public static void Log(log4net.Core.LoggingEvent logEvent)
        {
            Logger.Log(logEvent.Level, logEvent.MessageObject, logEvent.ExceptionObject);
        }

        public static void Log(System.Type callerStackBoundryDeclaringType, log4net.Core.Level level, object message, Exception exception)
        {
            Logger.instance = log4net.LogManager.GetLogger(callerStackBoundryDeclaringType);
            Logger.Log(level, message, exception);
        }

        /// <summary>
        /// Since this method's polymorphic cousin with 'string typeString' as the second argument is the prevalent choice used throughout the source base,
        /// and since the LoggerAppender unfortunately utilizes the prevalent choice to append messages using a custom interface, it was necessary to supplant
        /// the custom logger with a more log4net friendly version in order to take advantage of the rich features available, i.e. filters.
        /// </summary>
        /// <param name="level">The level.</param>
        /// <param name="message">The string message base</param>
        /// <param name="exception">The exception.</param>
        public static void Log(ErrorLevel errorLevel, object typeString, string message)
        {
            if (Logger.instance == null)
            {
                instance = log4net.LogManager.GetLogger(typeof(Logger));
            }
            if (typeString is String)
            {
                message = typeString + message;
            }
            switch (errorLevel.ToString())
            {
                case "INFO":
                    Logger.instance.Info(message);
                    break;
                case "DEBUG":
                    Logger.instance.Debug(message);
                    break;
                case "WARNING":
                    Logger.instance.Warn(message);
                    break;
                case "ERROR":
                    Logger.instance.Error(message);
                    break;
                default:
                    Logger.instance.Warn(message);
                    break;
            }
        }

#if LEGACY_LOG
        public static void Log(ErrorLevel errorLevel, string typeString, string message)
#else
        public static void realLog(ErrorLevel errorLevel, string typeString, string message)
#endif
        {
            var loggers = log4net.LogManager.GetCurrentLoggers();
            if (listener is TextWriterTraceListener)
            {
                ///string[] lines = message.Replace("\r\n", "\n").Replace('\r', '\n').Split('\n');
                StringBuilder msg = new StringBuilder(message);
                string[] lines = msg.Replace("\r\n", "\n").Replace('\r', '\n').ToString().Split('\n');
                msg.Clear();
                string threadName = System.Threading.Thread.CurrentThread.Name;
                string sessionID;
                string lineFormatString;
                StreamWriter streamWriter = null;
                if (string.IsNullOrEmpty(threadName) || !threadName.Contains("\0"))
                {
                    lineFormatString = normalLineFormatString;
                    sessionID = null;
                }
                else
                {
                    string fileName = SplitThreadName(out sessionID);
                    if (fileName != null)
                    {
                        string filePath = Path.Combine(fileLocation, fileNamePrefix + fileName + ".log");
                        try
                        {
                            streamWriter = File.AppendText(filePath);
                        }
                        catch { }
                    }

                    if (sessionID == null)
                    {
                        lineFormatString = normalLineFormatString;
                    }
                    else
                    {
                        lineFormatString = serviceLineFormatString;
                    }
                }
                string line = String.Format(lineFormatString, DateTime.Now.ToString(dateFormatString), errorLevel.ToString(), typeString, sessionID, lines[0]);
                if (streamWriter == null)
                {
                    Trace.WriteLine(line);
                }
                else
                {
                    try
                    {
                        streamWriter.WriteLine(line);
                    }
                    catch
                    {
                        Trace.WriteLine(line);
                        try
                        {
                            streamWriter.Close();
                        }
                        catch { }
                        finally
                        {
                            try
                            {
                                streamWriter.Dispose();
                            }
                            catch { }
                        }
                        streamWriter = null;
                    }
                }
                Console.WriteLine(line);
                for (int i = 1; i < lines.Length; i++)
                {
                    line = String.Format(lineFormatString, null, null, null, sessionID, lines[i]);
                    if (streamWriter == null)
                    {
                        Trace.WriteLine(line);
                    }
                    else
                    {
                        try
                        {
                            streamWriter.WriteLine(line);
                        }
                        catch
                        {
                            Trace.WriteLine(line);
                            try
                            {
                                streamWriter.Close();
                            }
                            catch { }
                            finally
                            {
                                try
                                {
                                    streamWriter.Dispose();
                                }
                                catch { }
                            }
                            streamWriter = null;
                        }
                    }
                    Console.WriteLine(line);
                }
                if (streamWriter == null)
                {
                    Trace.Flush();
                }
                else
                {
                    try
                    {
                        streamWriter.Close();
                    }
                    catch { }
                    finally
                    {
                        try
                        {
                            streamWriter.Dispose();
                        }
                        catch { }
                    }
                }
            }
            else
            {
                Log(errorLevel, (object)typeString, message);
            }
        }

        public static void Log(string senderType, string message, Exception exc)
        {
            Log(ErrorLevel.ERROR, senderType, message + "\n" + GetExceptionString(exc));
        }

        //public static void PromptUser(string senderType, string text, string caption)
        //{
        //    Log(ErrorLevel.DEBUG, senderType, "Showing to the user:\n" + text);
        //    Log(ErrorLevel.DEBUG, senderType, MessageBox.Show(text, caption).ToString());
        //}

        //public static DialogResult PromptUser(string senderType, string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        //{
        //    switch (buttons)
        //    {
        //        case MessageBoxButtons.OK:
        //            {
        //                Log(ErrorLevel.DEBUG, senderType, "Showing to the user:\n" + text);
        //                break;
        //            }
        //        default:
        //            {
        //                Log(ErrorLevel.DEBUG, senderType, "Prompting the user:\n" + text);
        //                break;
        //            }
        //    }
        //    DialogResult result = MessageBox.Show(text, caption, buttons, icon);
        //    Log(ErrorLevel.DEBUG, senderType, result.ToString());
        //    return result;
        //}

        public static string StandardizeThreadID(string threadID)
        {
            System.Text.RegularExpressions.Match match = 
                System.Text.RegularExpressions.Regex.Match(threadID, "urn:uuid:([0-9a-f]{8})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{4})-([0-9a-f]{12})", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            if (match.Success)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 1; i < 7; i++)
                {
                    sb.Append(match.Groups[i].Value);
                }
                string logstr = sb.ToString();
                sb.Clear();
                return logstr;
            }
            else
            {
                return threadID;
            }
        }

        public static string StandardizeThreadName(System.Security.Principal.WindowsIdentity clientIdentity, string sessionID)
        {
            string clientName = clientIdentity.Name;
            clientName = clientName.Substring(clientName.LastIndexOf('\\') + 1);
            return clientName + "\0" + StandardizeThreadID(sessionID);
        }

        public static string StandardizeThreadName(string serverName, string shareName, string caseName)
        {
            return string.Format("{0}_{1}_{2}{3}", serverName, shareName, caseName, "\0\0");
        }

        public static string StandardizeThreadName(string siteName)
        {
            lock(siteName)
                return string.Format("{0}{1}", siteName, "\0\0\0");
        }

        public static string SplitThreadName(out string sessionID)
        {
            string[] tokens = System.Threading.Thread.CurrentThread.Name.Split('\0');
            switch (tokens.Length)
            {
                case 3:
                    {
                        sessionID = null;
                        return tokens[0];
                    }
                case 4:
                    {
                        sessionID = tokens[0];
                        return null;
                    }
                default:
                    {
                        sessionID = tokens[1];
                        return tokens[0];
                    }
            }
        }

        //public static void LogCreateCase(ProgramType programType, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        //{
        //    string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=CreateCase";
        //    LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.CREATE_CASE, clientIdentity, message);
        //}

        //public static void LogDeleteCase(ProgramType programType, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        //{
        //    string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=DeleteCase";
        //    LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.DELETE_CASE, clientIdentity, message);
        //}

        //public static void LogRenameCase(ProgramType programType, ICase icase, string newName, string oldName, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        //{
        //    string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=RenameCase oldcase=" + Path.Combine(icase.DataStore.Path, oldName);
        //    LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.RENAME_CASE, clientIdentity, message);
        //}

        //public static void LogRenameDuplicateCase(ProgramType programType, ICase icase, string newName, string oldName, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        //{
        //    string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=RenameDuplicateCase oldcase=" + Path.Combine(icase.DataStore.Path, oldName);
        //    LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.RENAME_DUPLICATE_CASE, clientIdentity, message);
        //}

        //public static void LogResetPermissions(ProgramType programType, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        //{
        //    string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=ResetCasePermissions";
        //    LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.RESET_CASE_PERMISSIONS, clientIdentity, message);
        //}
#if GETPERMISSIONS
        public static void LogGetPermissions(ProgramType programType, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        {
            string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=GetCasePermissions";
            LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.GET_CASE_PERMISSIONS, clientIdentity, message);
        }

        public static void LogGetDatastorePermissions(ProgramType programType, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        {
            string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=GetDatastorePermissions";
            LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.GET_SHARE_PERMISSIONS, clientIdentity, message);
        }

        public static void LogGetCasePermissions(ProgramType programType, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
        {
            string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=GetPermissions";
            LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.GET_PERMISSIONS, clientIdentity, message);
        }
#endif
//        public static void LogAddGroupMember(ProgramType programType, string groupLabel, NetUI.ActiveDirectoryItem member, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
//        {
//            string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=Add" + groupLabel + " member=" + member.LogonNamePreWindows2000;
//            ActionType actionType;
//            switch (groupLabel.ToLower())
//            {
//                case "owner":
//                    {
//                        actionType = ActionType.ADD_OWNER;
//                        break;
//                    }
//                case "examiner":
//                    {
//                        actionType = ActionType.ADD_EXAMINER;
//                        break;
//                    }
//                case "reviewer":
//                    {
//                        actionType = ActionType.ADD_REVIEWER;
//                        break;
//                    }
//                default:
//                    {
//                        actionType = 0;
//                        break;
//                    }
//            }
//            LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, actionType, clientIdentity, message);
//        }
        
//        public static void LogRemoveGroupMember(ProgramType programType, string groupLabel, NetUI.ActiveDirectoryItem member, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
//        {
//            string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=Remove" + groupLabel + " member=" + member.LogonNamePreWindows2000;
//            ActionType actionType;
//            switch (groupLabel.ToLower())
//            {
//                case "owner":
//                    {
//                        actionType = ActionType.REMOVE_OWNER;
//                        break;
//                    }
//                case "examiner":
//                    {
//                        actionType = ActionType.REMOVE_EXAMINER;
//                        break;
//                    }
//                case "reviewer":
//                    {
//                        actionType = ActionType.REMOVE_REVIEWER;
//                        break;
//                    }
//                default:
//                    {
//                        actionType = 0;
//                        break;
//                    }
//            }
//            LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, actionType, clientIdentity, message);
//        }

//        public static void LogMountCase(ICase icase)
//        {
//            LogMountCase(icase.CATConfig, ProgramType.CASE_MOUNTER, icase, icase.CATConfig.UserMembership.UserToken, icase.CATConfig.UserMembership);
//        }

//        public static void LogMountCase(CATConfiguration catConfiguration, ProgramType programType, ICase icase, System.Security.Principal.WindowsIdentity clientIdentity, UserMembership clientMembership)
//        {
//            string message = "user=" + clientMembership.ADI.LogonNamePreWindows2000 + " action=MountCase";
//            LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, programType, ActionType.MOUNT_CASE, clientIdentity, message);
//        }

//        public static void LogUnmountCase(ICase icase)
//        {
//            string message = "user=" + icase.CATConfig.UserMembership.ADI.LogonNamePreWindows2000 + " action=UnmountCase";
//            LogEvent(icase, UnmanagedCode.PInvoke.EventType.Information, ProgramType.CASE_MOUNTER, ActionType.UNMOUNT_CASE, icase.CATConfig.UserMembership.UserToken, message);
//        }

//        public static void LogEvent(ICase icase, UnmanagedCode.PInvoke.EventType eventType, ProgramType programType, ActionType actionType, System.Security.Principal.WindowsIdentity clientIdentity, string message)
//        {
//            if (icase != null)
//            {
//                message += string.Format(" case={0} tool={1} version={2}", icase != null ? icase.Path : "NA", currentExeName, currentExeVersion);
//                LogEventToDC(CATService.serviceName, icase.CATConfig.EventLogName, icase.CATConfig.EventLogMachine, eventType, programType, actionType, clientIdentity, message);
//                switch (actionType)
//                {
//                    case ActionType.DELETE_CASE:
//                        {
//                            //obviously can't log this to the file
//                            break;
//                        }
//                    default:
//                        {
//                            LogEventToLocalLog(icase, message);
//                            break;
//                        }
//                }
//            }
//        }

//        public static bool LogEventToDC(string sourceName, string logName, string machineName, UnmanagedCode.PInvoke.EventType eventType, ProgramType programType, ActionType actionType, System.Security.Principal.WindowsIdentity userIdentity, params string[] messages)
//        {
//            int lastError;
//            Logger.Log(ErrorLevel.DEBUG, "EventLog", string.Format("Attempting to write to the event log: {0}\\{1} with source: {2}, with message: {3}", machineName, logName, sourceName, string.Join(";", messages)));
//            if (!UnmanagedCode.PInvoke.LogEvent(out lastError, sourceName, logName, machineName, eventType, (ushort)programType, (uint)actionType, userIdentity, messages))
//            {
//                Exception logExc = new System.ComponentModel.Win32Exception(lastError);
//                string compDomain = UnmanagedCode.PInvoke.GetLocalComputerDNSDomainName();
//                if (compDomain != null && compDomain.ToLower().Contains(machineName.ToLower()))
//                {
//                    UnmanagedCode.PInvoke.DOMAIN_CONTROLLER_INFO dcInfo = UnmanagedCode.PInvoke.GetDomainInfo(null, machineName, null, 0);
//                    string newMachineName = dcInfo.DomainControllerName.TrimStart('\\');
//                    int secondError;
//                    Logger.Log(ErrorLevel.DEBUG, "EventLog", string.Format("Attempting to write to the event log: {0}\\{1} with source: {2}, with message: {3}", newMachineName, logName, sourceName, string.Join(";", messages)));
//                    if (!UnmanagedCode.PInvoke.LogEvent(out secondError, sourceName, logName, newMachineName, eventType, (ushort)programType, (uint)actionType, userIdentity, messages))
//                    {
//                        Exception tempExc = new System.ComponentModel.Win32Exception(secondError);
//                        logExc = new System.ComponentModel.Win32Exception(tempExc.Message, logExc);
//                    }
//                    else
//                    {
//                        return true;
//                    }
//                }

//                Logger.Log("EventLog", "Event Log write failed:", logExc);
//                return false;
//            }
            
//            return true;
//        }

//        public static bool LogEventToLocalLog(ICase icase, string message)
//        {
//            Exception fatalException;
//            try
//            {
//                FileInfo logFile = new FileInfo(Path.Combine(icase.Path, CATConfiguration.LocalCaseLogFileName));
//                string fullMessage = string.Format("{0} {1}", DateTime.Now.ToString(dateFormatString), message);
//                Logger.Log(ErrorLevel.DEBUG, "LocalLog", string.Format("Attempting to write to the case log: {0}, with message: {1}", logFile.FullName, fullMessage));

//                //make sure the file exists with the right permissions
//                try
//                {
//                    if (!logFile.Exists)
//                    {
//                        CommonMethods.ResetActionsLogPermissions(logFile,
//                            icase.OwnerGroup == null ? null : icase.OwnerGroup.IdentityRef,
//                            icase.ExaminerGroup == null ? null : icase.ExaminerGroup.IdentityRef,
//                            icase.ReviewerGroup.IdentityRef);
//                    }
//                    fatalException = null;
//                }
//                catch (Exception createExc)
//                {
//                    fatalException = createExc;
//                }

//                for (int i = 0; i < 2; i++)
//                {
//                    try
//                    {
//                        IntPtr pFile = UnmanagedCode.PInvoke.CreateFile(logFile.FullName, UnmanagedCode.PInvoke.FileAccessRights.AppendData, FileShare.ReadWrite, FileMode.Open, FileOptions.None);
//                        using (SafeFileHandle safeHandle = new SafeFileHandle(pFile, true))
//                        {
//                            using (StreamWriter sw = new StreamWriter(new FileStream(safeHandle, FileAccess.Write)))
//                            {
//                                sw.WriteLine(fullMessage);
//                                sw.Close();
//                            }
//                            safeHandle.Close();
//                            return true;
//                        }
//                    }
//                    catch (Exception writeExc)
//                    {
//                        fatalException = writeExc;
//                    }
//                }
//            }
//            catch (Exception fatalExc)
//            {
//                fatalException = fatalExc;
//            }
//            Logger.Log("LocalLog", "Local Log failed:", fatalException);
//            return false;
//        }

//        public enum ActionType
//        {
//            CREATE_CASE = 0x1,
//            DELETE_CASE,
//            RENAME_CASE,
//            RENAME_DUPLICATE_CASE,
//            RESET_CASE_PERMISSIONS,
//            ADD_OWNER,
//            ADD_EXAMINER,
//            ADD_REVIEWER,
//            REMOVE_OWNER,
//            REMOVE_EXAMINER,
//            REMOVE_REVIEWER,
//            MOUNT_CASE,
//            UNMOUNT_CASE,
//#if GETPERMISSIONS
//            GET_CASE_PERMISSIONS,
//            GET_SHARE_PERMISSIONS,
//            GET_PERMISSIONS
//#endif
//        }

//        public enum ProgramType : ushort
//        {
//            CAT_CLIENT = 0x1,
//            CAT_SERVICE,
//            CASE_MOUNTER,
//        }
    }
}
