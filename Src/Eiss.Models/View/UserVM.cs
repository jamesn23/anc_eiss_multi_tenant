﻿using Eiss.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Models.View
{
    public class UserVM : IEntityBase
    {
        public int Id { get; set; }
        //public long? SiteId { get; set; }
        //public SiteDM Site { get; set; }
        public long? RoleId { get; set; }
        //public RoleDM Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public bool IsApproved { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastLoginDate { get; set; }
    }

    public class LoginVM
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
