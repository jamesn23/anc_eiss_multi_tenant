﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Tenant
{
    /// <summary>
    /// Represents a common Tenant
    /// </summary>
    public class Tenant : ITenant
    {
        /// <summary>
        /// Tenant's Id
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Tenant's name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The Tenant domains
        /// </summary>
        public string HostUrl { get; private set; }

        /// <summary>
        /// The Tenant Theme
        /// </summary>
        public string Theme { get; private set; }

        /// <summary>
        /// Properties collection
        /// </summary>
        public Dictionary<string, List<object>> Properties { get; private set; }

        /// <summary>
        /// The Resolver instance
        /// </summary>
        public ITenantResolver Resolver { get; set; }

        /// <summary>
        /// Creates a new Tenant
        /// </summary>
        /// <param name="id">The ID</param>
        /// <param name="name">The name</param>
        /// <param name="domain">The related Domain</param>
        /// <param name="theme">the set theme</param>
        public Tenant(int id, string name, string hostUrl,string theme)
        {
            Id = id;
            Name = name;
            HostUrl = hostUrl;
            Theme = theme;
            Properties = new Dictionary<string,List<object>>();
        }
    }
}
