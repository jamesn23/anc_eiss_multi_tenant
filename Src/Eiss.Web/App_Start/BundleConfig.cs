﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using Eiss.Tenant;

namespace Eiss.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/Vendors/modernizr.js"));

            bundles.Add(new ScriptBundle("~/bundles/vendors").Include(
                "~/Scripts/Vendors/jquery.js",
                "~/Scripts/Vendors/bootstrap.js",
                "~/Scripts/Vendors/toastr.js",
                "~/Scripts/Vendors/jquery.raty.js",
                "~/Scripts/Vendors/respond.src.js",
                "~/Scripts/Vendors/angular.js",
                "~/Scripts/Vendors/angular-route.js",
                "~/Scripts/Vendors/angular-cookies.js",
                "~/Scripts/Vendors/angular-validator.js",
                "~/Scripts/Vendors/angular-base64.js",
                "~/Scripts/Vendors/angular-file-upload.js",
                "~/Scripts/Vendors/angucomplete-alt.min.js",
                "~/Scripts/Vendors/angular-local-storage.min.js",
                "~/Scripts/Vendors/ui-bootstrap-tpls-0.13.1.js",
                "~/Scripts/Vendors/underscore.js",
                "~/Scripts/Vendors/raphael.js",
                "~/Scripts/Vendors/morris.js",
                "~/Scripts/Vendors/jquery.fancybox.js",
                "~/Scripts/Vendors/jquery.fancybox-media.js",
                "~/Scripts/Vendors/loading-bar.js",
                //kendo scripts
                "~/Scripts/Vendors/kendo.all.min.js",
                "~/Scripts/Vendors/kendo.web.min.js",
                "~/Scripts/Vendors/jszip.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/spa").Include(
                "~/Scripts/spa/modules/common.core.js",
                "~/Scripts/spa/modules/common.ui.js",
                "~/Scripts/spa/app.js",
                //"~/Scripts/spa/services/apiService.js",
                "~/Scripts/spa/services/notificationService.js",
                "~/Scripts/spa/services/accountService.js",
                "~/Scripts/spa/services/authInterceptService.js",
                //"~/Scripts/spa/services/fileUploadService.js",
                "~/Scripts/spa/layout/topBar.directive.js",
                "~/Scripts/spa/layout/sideBarSite.directive.js",
                "~/Scripts/spa/layout/sideBarEmpty.directive.js",
                "~/Scripts/spa/layout/sideBarPortal.directive.js",
                "~/Scripts/spa/layout/customPager.directive.js",
                //"~/Scripts/spa/directives/rating.directive.js",
                //"~/Scripts/spa/directives/availableMovie.directive.js",
                "~/Scripts/spa/account/loginCtrl.js",
                "~/Scripts/spa/account/registerCtrl.js",
                "~/Scripts/spa/home/rootCtrl.js",
                "~/Scripts/spa/home/indexCtrl.js",
                "~/Scripts/spa/portal/portalindexCtrl.js"
                //"~/Scripts/spa/customers/customersCtrl.js",
                //"~/Scripts/spa/burialRequest/burialRequestCtrl.js",
                //"~/Scripts/spa/burialRequest/decedentCtrl.js",
                //"~/Scripts/spa/burialRequest/govtSvcRecordCtrl.js",
                //"~/Scripts/spa/interments/intermentCtrl.js",
                //"~/Scripts/spa/customers/customersRegCtrl.js",
                //"~/Scripts/spa/customers/customerEditCtrl.js",
                //"~/Scripts/spa/movies/moviesCtrl.js",
                //"~/Scripts/spa/movies/movieAddCtrl.js",
                //"~/Scripts/spa/movies/movieDetailsCtrl.js",
                //"~/Scripts/spa/movies/movieEditCtrl.js",
                //"~/Scripts/spa/controllers/rentalCtrl.js",
                //"~/Scripts/spa/rental/rentMovieCtrl.js",
                //"~/Scripts/spa/rental/rentStatsCtrl.js"
                ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/content/css/site.css",
                "~/content/css/bootstrap.css",
                //"~/content/css/bootstrap-theme.css",//let the site set its own themes 
                 "~/content/css/font-awesome.css",
                "~/content/css/morris.css",
                "~/content/css/toastr.css",
                "~/content/css/jquery.fancybox.css",
                "~/content/css/loading-bar.css"
                 //"~/content/css/eiss-global.css",
                //"~/content/css/personStyles.css"
                ));

            bundles.Add(new StyleBundle("~/Content/kendo").Include(
               "~/Content/kendo/kendo.common.min.css",
               "~/Content/kendo/kendo.default.min.css"
               ));

            BundleTable.EnableOptimizations = false;
        }
    }
}