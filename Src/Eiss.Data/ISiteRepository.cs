﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Data
{
    public interface ISiteRepository<T> : IRepository<T>
    {
        /// <summary>
        /// get all sites in db
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> Select();
    }
}
