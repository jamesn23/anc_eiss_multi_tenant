﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Data
{
    /// <summary>
    /// all domain models should inherit
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEntityQuery<T>
    {
        /// <summary>
        /// select all query for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        string SelectQuery(int siteId);

        /// <summary>
        /// select single query for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        string SelectQuery(int siteId, int id);

        /// <summary>
        /// insert query for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        string InsertQuery(int siteId, T entity);

        /// <summary>
        /// update query for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        string UpdateQuery(int siteId, T entity);

        /// <summary>
        /// upsert query for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        string UpsertQuery(int siteId, T entity);

        /// <summary>
        /// delete query for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        string DeleteQuery(int siteId, int id);
      
    }
}
