﻿using Eiss.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Models.Data
{
    public class UserDM : IEntityBase, IEntityQuery<UserDM>
    {
        public int Id { get; set; }
        public long? SiteId { get; set; }
        //public SiteDM Site { get; set; }
        //public long? RoleId { get; set; }
        public RoleDM Role { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public bool IsApproved { get; set; }
        public bool IsLocked { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastLoginDate { get; set; }

        //Need to integrate sit id so no users are leaking!!!!!

        ///Queries
        /// <summary>
        /// select all users across all sites
        /// </summary>
        /// <returns></returns>
        //public string SelectQuery()
        //{
        //    return @"SELECT usr.Id as Id, usr.FirstName, usr.LastName, usr.Email, usr.IsApproved, usr.IsLocked, 
        //                usr.DateCreated, usr.LastLoginDate, rle.Id as RoleId, rle.Name as RoleName 
        //                FROM[dbo].[tbl_User] usr
        //                LEFT JOIN[dbo].[lu_tbl_Role] [rle] ON[rle].Id = usr.RoleId;";
        //}

        /// <summary>
        /// Select all users in the current site
        /// </summary>
        /// <param name="siteId">Specifies the site</param>
        /// <returns></returns>
        public string SelectQuery(int siteId)
        {
            StringBuilder sb = new StringBuilder(@"SELECT usr.Id as UserId, usr.FirstName, usr.LastName, usr.Email,
                usr.[Password], usr.IsApproved, usr.IsLocked, usr.DateCreated, usr.LastLoginDate, rle.Id as RoleId, 
                rle.Name as RoleName 
                FROM[dbo].[tbl_User] usr 
                LEFT JOIN[dbo].[lu_tbl_Role][rle] ON[rle].Id = usr.RoleId
                LEFT JOIN[dbo].[jtbl_User_Sites][usrsite] ON[usrsite].UserId = usr.Id ");
            sb.AppendFormat("WHERE [usrsite].SiteId = {0}", siteId);
            return sb.ToString();
        }

        public string SelectQuery(int siteId, int id)
        {
            StringBuilder sb = new StringBuilder(@"SELECT usr.Id as UserId, usr.FirstName, usr.LastName, usr.Email, usr.[Password], 
                usr.IsApproved, usr.IsLocked, usr.DateCreated, usr.LastLoginDate, usr.PasswordSalt as Salt, rle.Id as RoleId, rle.Name as RoleName
                FROM[dbo].[tbl_User] usr 
                LEFT JOIN[dbo].[lu_tbl_Role] [rle] ON[rle].Id = usr.RoleId
                LEFT JOIN[dbo].[jtbl_User_Sites][usrsite] ON[usrsite].UserId = usr.Id ");
            sb.AppendFormat("WHERE [usrsite].SiteId = {0} AND usr.Id = {1}", siteId, id);
            return sb.ToString();
        }

        public string SelectQuery(string email)
        {
            StringBuilder sb = new StringBuilder(@"SELECT usr.Id as UserId, usr.FirstName, usr.LastName, usr.Email, usr.[Password],
                usr.IsApproved, usr.IsLocked, usr.DateCreated, usr.LastLoginDate, usr.PasswordSalt as Salt, rle.Id as RoleId, rle.Name as RoleName
                FROM [dbo].[tbl_User] usr 
                LEFT JOIN [dbo].[lu_tbl_Role] [rle] ON [rle].Id = usr.RoleId ");
            sb.AppendFormat("WHERE usr.Email = '{0}'", email.ToUpper().Trim());
            return sb.ToString();
        }

        public string SelectQuery(int siteId, string email)
        {
            StringBuilder sb = new StringBuilder(@"SELECT usr.Id as UserId, usr.FirstName, usr.LastName, usr.Email, usr.[Password],
                usr.IsApproved, usr.IsLocked, usr.DateCreated, usr.LastLoginDate, usr.PasswordSalt as Salt, rle.Id as RoleId, rle.Name as RoleName
                FROM [dbo].[tbl_User] usr 
                LEFT JOIN [dbo].[lu_tbl_Role] [rle] ON [rle].Id = usr.RoleId
                LEFT JOIN[dbo].[jtbl_User_Sites][usrsite] ON[usrsite].UserId = usr.Id ");
            sb.AppendFormat("WHERE [usrsite].SiteId = {0} AND usr.Email = '{1}'", siteId, email.ToUpper().Trim());
            return sb.ToString();
        }

        public string InsertQuery(int siteId, UserDM entity)
        {
            StringBuilder sb = new StringBuilder(@"DECLARE @currId INT; INSERT INTO [dbo].[tbl_User](RoleId, FirstName, LastName,
                Email,[Password], IsApproved, IsLocked, DateCreated, LastLoginDate, PasswordSalt) ");
            sb.AppendFormat("VALUES(2, ");
            sb.AppendFormat("'{0}', ", entity.FirstName);
            sb.AppendFormat("'{0}', ", entity.LastName);
            sb.AppendFormat("'{0}', ", entity.Email);
            sb.AppendFormat("'{0}', ", entity.Password);
            sb.AppendFormat("'{0}', ", entity.Salt);
            sb.AppendFormat(@"'False', 'True', GETDATE(), GETDATE()) SET @currId = SCOPE_IDENTITY() INSERT INTO
                [dbo].[jtbl_User_Sites](UserId, SiteId) VALUES(@currId, {0});",siteId);
            //sb.AppendFormat(");");
            return sb.ToString();
        }

        public string InsertQuery(int siteId, int id)
        {
            StringBuilder sb = new StringBuilder("INSERT INTO [dbo].[jtbl_User_Sites](UserId, SiteId) ");
            sb.AppendFormat("VALUES({0}, {1});", id,siteId);
            //sb.AppendFormat(");");
            return sb.ToString();
        }

        public string UpdateQuery(int siteId, UserDM entity)
        {
            StringBuilder sb = new StringBuilder("UPDATE [dbo].[tbl_User] SET ");
            sb.AppendFormat("FirstName = '{0}',", entity.FirstName);
            sb.AppendFormat("LastName = '{0}',", entity.LastName);
            sb.AppendFormat("Email = '{0}',", entity.Email);
            sb.AppendFormat("IsApproved = '{0}',", entity.IsApproved);
            sb.AppendFormat("Islocked = '{0}',", entity.IsLocked);
            sb.AppendFormat("WHERE Id = {0}", entity.Id);
            return sb.ToString();
        }

        public string UpsertQuery(int siteId, UserDM entity)
        {
            throw new NotImplementedException();
        }

        public string DeleteQuery(int siteId, int id)
        {
            return string.Format("DELETE FROM [dbo].jtbl_User_Sites WHERE UserId = {0}; DELETE FROM [dbo].tbl_User WHERE Id = {0}", id);
        }
    }

}
