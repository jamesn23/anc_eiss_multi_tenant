﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using Eiss.Web.App_Start;
using System.Web.Optimization;
using System.Web.Helpers;
using System.Security.Claims;

namespace Eiss.Web
{
    public class Global : HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            var config = GlobalConfiguration.Configuration;

            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(config);
            Bootstrapper.Run();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configuration.EnsureInitialized();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           
            //This tells the AntiForgery class to use the NameIdentifier(which is the user id string found by GetUserId)
            //AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Email;

        }

//        protected void Application_BeginRequest()
//        {
//#if !DEBUG
//           // SECURE: Ensure any request is returned over SSL/TLS in production
//           if (!Request.IsLocal && !Context.Request.IsSecureConnection) {
//               var redirect = Context.Request.Url.ToString().ToLower(CultureInfo.CurrentCulture).Replace("http:", "https:");
//               Response.Redirect(redirect);
//           }
//#endif
//        }
    }
}