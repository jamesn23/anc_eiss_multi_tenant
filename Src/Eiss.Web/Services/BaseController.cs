﻿using AutoMapper;
using Eiss.Data;
using Eiss.Tenant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Eiss.Web.Services
{
    /// <summary>
    /// Controller Definitions
    /// </summary>
    /// <typeparam name="RP">Repository</typeparam>
    /// <typeparam name="DM">Data Model</typeparam>
    /// <typeparam name="VM">ViewModel</typeparam>
    public class BaseController<RP, DM, VM> : ApiController where RP : IRepository<DM> where VM : IEntityBase
    {
        protected RP _repository;
        //retrieve the current site 
        protected ITenant _tenant => Request.GetOwinContext().GetCurrentTenant();
        protected void InitalizeMappings() { Mapper.Initialize(x => { x.CreateMap<DM, VM>(); x.CreateMap<VM, DM>(); }); }

        [HttpGet]
        public virtual IHttpActionResult Get(HttpRequestMessage request)
        {
            //now make call to get the data
            IEnumerable<VM> result = Mapper.Map<IEnumerable<DM>, IEnumerable<VM>>(_repository.Select(_tenant.Id)); ///USE AUTOMAPPER HERE!!!

            if (result == null)
            {
                var errNotFound = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(string.Format("No records found!")),
                    ReasonPhrase = "Not Found"
                };
                throw new HttpResponseException(errNotFound);
            }

            return Ok(result);
        }

        //not used
        [HttpGet]
        public virtual IHttpActionResult Get(HttpRequestMessage request, int id)
        {
            //now make call to get the data
            VM result = Mapper.Map<DM, VM>(_repository.Select(_tenant.Id, id)); ///USE AUTOMAPPER HERE!!!

            if (result == null)
            {
                var errNotFound = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(string.Format("record not found!")),
                    ReasonPhrase = "Not Found"
                };
                throw new HttpResponseException(errNotFound);
            }

            return Ok(result);
        }

        [HttpPost]
        public virtual IHttpActionResult Post(HttpRequestMessage request, VM entity)
        {
            //need to verify model state fist before anything
            if (!ModelState.IsValid)
            {
                var errBadRequest = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("Bad Request")),
                    ReasonPhrase = "Bad Request"
                };
                throw new HttpResponseException(errBadRequest);
            }

            var result = _repository.Insert(_tenant.Id, Mapper.Map<VM, DM>(entity)); ///USE AUTOMAPPER HERE!!!

            if (result < 1)
            {
                var errConflict = new HttpResponseMessage(HttpStatusCode.Conflict)
                {
                    Content = new StringContent(string.Format("there was an issue creating record")),
                    ReasonPhrase = "Conflict"
                };
                throw new HttpResponseException(errConflict);
            }

            return Ok(result);
            //return CreatedAtRoute("DefaultApi", new { id = entity.Id}, entity);
        }

        [HttpPut]
        public virtual IHttpActionResult Update(HttpRequestMessage request, VM entity)
        {
            //need to verify model state fist before anything
            if (!ModelState.IsValid)
            {
                var errBadRequest = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("Bad Request")),
                    ReasonPhrase = "Bad Request"
                };
                throw new HttpResponseException(errBadRequest);
            }

            var result = _repository.Update(_tenant.Id, Mapper.Map<VM, DM>(entity)); ///USE AUTOMAPPER HERE!!!

            if (result == false)
            {
                var errConflict = new HttpResponseMessage(HttpStatusCode.Conflict)
                {
                    Content = new StringContent(string.Format("there was an isse updating record")),
                    ReasonPhrase = "Conflict"
                };
                throw new HttpResponseException(errConflict);
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [HttpDelete]
        public virtual IHttpActionResult Delete(HttpRequestMessage request, int id)
        {
            var result = _repository.Delete(_tenant.Id, id); ///USE AUTOMAPPER HERE!!!

            if (result == false)
            {
                var errNotFound = new HttpResponseMessage(HttpStatusCode.Conflict)
                {
                    Content = new StringContent(string.Format("there was an issue updating record")),
                    ReasonPhrase = "Conflict"
                };
                throw new HttpResponseException(errNotFound);
            }

            return Ok(result);
        }
    }
}
