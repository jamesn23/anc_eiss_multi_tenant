﻿using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Tenant
{
    /// <summary>
    /// Tenant Resolver class using request's hostname as key to find
    /// a possible Tenant
    /// </summary>
    public class HostNameResolver : ResolverBase, ITenantResolver
    {
        /// <summary>
        /// Instances a new Resolver without tenants
        /// </summary>
        public HostNameResolver() : base()
        {
        }

        /// <summary>
        /// Instances a new resolver with a tenant list
        /// </summary>
        /// <param name="tenants">The list of tenants</param>
        /// <param name="databaseIdentifier">The Database Identifier</param>
        public HostNameResolver(IEnumerable<ITenant> tenants)
            : base(tenants)
        {
        }

        /// <summary>
        /// Resolves a tenant request using hostname
        /// </summary>
        /// <param name="request">The request</param>
        /// <returns>The tenant or null</returns>
        public ITenant Resolve(IOwinRequest request)
        {
            return Tenants.FirstOrDefault(x => x.HostUrl == request.Uri.Host);
        }
    }
}
