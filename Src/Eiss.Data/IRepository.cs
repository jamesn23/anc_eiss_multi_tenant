﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Data
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Get all for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        IEnumerable<T> Select(int siteId);

        /// <summary>
        /// Get single for site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        T Select(int siteId, int id);

        /// <summary>
        /// Insert entity for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        int Insert(int siteId,T entity);

        /// <summary>
        /// Update entity for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Update(int siteId, T entity);

        /// <summary>
        /// Upsert entity for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        bool Upsert(int siteId, T entity);

        /// <summary>
        /// Delete entity for current site
        /// </summary>
        /// <param name="siteId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        bool Delete(int siteId, int id);
    }
}
