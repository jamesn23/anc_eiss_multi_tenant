﻿using AutoMapper;
using Eiss.Data;
using Eiss.Data.Msss;
using Eiss.Models.Data;
using Eiss.Models.View;
using Eiss.Web.Services.Infrastructure.Account;
using Eiss.Web.Services.Infrastructure.Attribute;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Eiss.Web.Services.Api
{
    /// <summary>
    /// No need to define basic methods thats what the base is for
    /// this is an extension
    /// this was witten befor multi-tenancy so we will probably 
    /// need to some how link this up with the current Tenant/site not sure
    /// </summary>
    //[RequireHttps]//try and get this to work
    [RoutePrefix("api/Account")]
    public class AccountController : BaseController<IUserRepository<UserDM>, UserDM, UserVM>
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

        private IAuthenticationManager AuthenticationManager => Request.GetOwinContext().Authentication;

        private IAccountService account; 

        public AccountController(IUserRepository<UserDM> repository,IAccountService _account)
        {
            this._repository = repository;
            this.account = _account;
            
            //this works with one request at a time multiple has issues
            //if we need to lets swith it out with a less dynamic one.
            this.InitalizeMappings();
        }

        //we ar using our oauth bearerservice to authorize login so one it done well remove
        //[AllowAnonymous]
        //[HttpPost]
        //[ApiValidateAntiForgeryToken]//since we are not using mvc as ui we had to customize for angular ui,
        //[Route("authenticate")]
        //[AllowXRequestsEveryXSeconds(Message = "Too many attempts, please try again later", Name = "Throttle", Seconds = 60)]//we may need or we may not 
        //public async Task<IHttpActionResult> Login(HttpRequestMessage request, LoginVM user)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        var errBadRequest = new HttpResponseMessage(HttpStatusCode.BadRequest)
        //        {
        //            Content = new StringContent(string.Format("Bad Request")),
        //            ReasonPhrase = "Bad Request"
        //        };
        //        throw new HttpResponseException(errBadRequest);
        //    }

        //    var result = ValidateUser(user.Email, user.Password);

        //    if (result == null)
        //    {
        //        var errNotFound = new HttpResponseMessage(HttpStatusCode.NotFound)
        //        {
        //            Content = new StringContent(string.Format("record not found!")),
        //            ReasonPhrase = "Not Found"
        //        };
        //        throw new HttpResponseException(errNotFound);
        //    }

        //    try
        //    {
        //        //try to authenicate
        //        await SignInAsync(result);
        //        return Ok(new { success = true });
        //    }
        //    catch (Exception e)
        //    {
        //        //if any issues just return bad request
        //        return BadRequest(e.Message);
        //    }

        //}

        [AllowAnonymous]
        [HttpPost]
        [ApiValidateAntiForgeryToken]//since we are not using mvc as ui we had to customize for angular ui,
        [Route("registraton")]
        public IHttpActionResult Register(HttpRequestMessage request, UserVM user)
        {
            if (!ModelState.IsValid)
            {
                var errBadRequest = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("Bad Request")),
                    ReasonPhrase = "Bad Request"
                };
                throw new HttpResponseException(errBadRequest);
            }

            //search current site for user
            var existingUser = _repository.Select(_tenant.Id, user.Email);
            //if user exists on site tell them to sign in
            if (existingUser != null)
            {
                var errUsrFound = new HttpResponseMessage(HttpStatusCode.Conflict)
                {
                    Content = new StringContent(string.Format("User already exists for this site. Please sign In")),
                    ReasonPhrase = "Conflict"
                };
                throw new HttpResponseException(errUsrFound);
            }

            //search entire system for user
            existingUser = _repository.Select(user.Email);
            //if user exists tell them to import account instead of creating dups
            //0r we can modify to create diffent data bases for different sites
            if (existingUser != null)
            {
                var errUsrFound = new HttpResponseMessage(HttpStatusCode.Conflict)
                {
                    Content = new StringContent(string.Format("user was found in system. To continue please import account!")),
                    ReasonPhrase = "Conflict"
                };
                throw new HttpResponseException(errUsrFound);
            }

            //if all checks pass lets allow user creation
            var result = account.CreateUser(user, _repository,_tenant.Id);

            if (result == null)
            {
                var errNotFound = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(string.Format("record not found!")),
                    ReasonPhrase = "Not Found"
                };
                throw new HttpResponseException(errNotFound);
            }
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost]
        [ApiValidateAntiForgeryToken]//since we are not using mvc as ui we had to customize for angular ui,
        [Route("importuser")]
        public IHttpActionResult ImportUser(HttpRequestMessage request, LoginVM user)
        {
            if (!ModelState.IsValid)
            {
                var errBadRequest = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("Bad Request")),
                    ReasonPhrase = "Bad Request"
                };
                throw new HttpResponseException(errBadRequest);
            }

            //search current site for user
            var existingUser = _repository.Select(_tenant.Id, user.Email);
            //if user exists on site tell them to sign in
            if (existingUser != null)
            {
                var errUsrFound = new HttpResponseMessage(HttpStatusCode.Forbidden)
                {
                    Content = new StringContent(string.Format("User already exists for this site. Please sign In")),
                    ReasonPhrase = "Not Allowed"
                };
                throw new HttpResponseException(errUsrFound);
            }

            //search entire system for user
            var result = account.ValidateUser(user.Email, user.Password,_repository);

            if (result == null)
            {
                var errNotFound = new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new StringContent(string.Format("user not found in system. To continue please create an account or verify credentials ar correct!")),
                    ReasonPhrase = "Not Found"
                };
                throw new HttpResponseException(errNotFound);
            }

            //TO-DO: now add the user to the current site
            account.AddExistingUser(result,_repository,_tenant.Id);

            return Ok(result);
        }
    }
}
