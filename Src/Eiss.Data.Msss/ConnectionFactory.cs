﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Data.Msss
{
    public static class ConnectionFactory
    {
        public static string anc_eiss_connection = ConfigurationManager.ConnectionStrings["ANC_EISS_Connection"].ConnectionString;

        public static Func<DbConnection> AncEissConnection = () => new SqlConnection(anc_eiss_connection);
        //public static Func<DbConnection> EISSMBConnectionFactory = () => new SqlConnection(eissmbConnection);
    }
}
