﻿using Eiss.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Models.Data
{
    public class SiteDM : IEntityBase, IEntityQuery<SiteDM>
    {
        public int Id { get; set; }
        //public PortalDM Portal { get; set; }
        public string Name { get; set; }
        public string HostUrl { get; set; }
        public string Theme { get; set; }
        public DateTime DateCreated { get; set; }
        //public IEnumerable<UserDM> Users { get; set; }
        public IEnumerable<PageDM> Pages { get; set; }

        public string SelectQuery()
        {
           return "SELECT Id, Name, DateCreated, HostUrl, Theme FROM [dbo].[tbl_Site];";
        }

        public string SelectQuery(int siteId)
        {
            throw new NotImplementedException();
        }

        public string SelectQuery(int siteId, int id)
        {
            throw new NotImplementedException();
        }

        public string InsertQuery(int siteId, SiteDM entity)
        {
            throw new NotImplementedException();
        }

        public string UpdateQuery(int siteId, SiteDM entity)
        {
            throw new NotImplementedException();
        }

        public string UpsertQuery(int siteId, SiteDM entity)
        {
            throw new NotImplementedException();
        }

        public string DeleteQuery(int siteId, int id)
        {
            throw new NotImplementedException();
        }
    }
}
