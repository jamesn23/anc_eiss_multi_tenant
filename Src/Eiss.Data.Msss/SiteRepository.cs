﻿using Dapper;
using Eiss.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Data.Msss
{
    public class SiteRepository : BaseRepositoryMsss<SiteDM>, ISiteRepository<SiteDM>
    {
        /// <summary>
        /// base select all implimentation for list of all sites
        /// </summary>
        /// <param name="siteId"></param>
        /// <returns></returns>
        public virtual IEnumerable<SiteDM> Select()
        {
            using (anc_eiss_connection = ConnectionFactory.AncEissConnection())
            {
                anc_eiss_connection.Open();

                var entity = anc_eiss_connection.Query<SiteDM>(Entity.SelectQuery()).ToList();
                return entity;
            }
        }
    }
}
