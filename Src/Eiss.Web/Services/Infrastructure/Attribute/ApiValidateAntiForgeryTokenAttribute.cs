﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Eiss.Web.Services.Infrastructure.Attribute
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public sealed class ApiValidateAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
    {
        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            try
            {
                //as long as its not a get request we will try to validate token
                if (actionContext.Request.Method.Method != "GET")
                {
                    HttpRequestHeaders headers = actionContext.Request.Headers;
                    CookieState tokenCookie = headers.GetCookies().Select(c => c[AntiForgeryConfig.CookieName]).FirstOrDefault();
                    string requestTkn = "X-XSRF-Token";
                    string tokenHeader = string.Empty;

                    //check request header for the token coming from spa
                    if (headers.Contains(requestTkn))
                    {
                        tokenHeader = headers.GetValues("X-XSRF-Token").FirstOrDefault();
                    }
                    //then finally lets try to validate
                    AntiForgery.Validate(tokenCookie != null ? tokenCookie.Value : null, tokenHeader);
                }
            }
            catch
            {
                actionContext.Response = new HttpResponseMessage
                {
                    StatusCode = HttpStatusCode.Forbidden,
                    RequestMessage = actionContext.ControllerContext.Request
                };
                return FromResult(actionContext.Response);
            }
            return continuation();
        }

        private Task<HttpResponseMessage> FromResult(HttpResponseMessage result)
        {
            var source = new TaskCompletionSource<HttpResponseMessage>();
            source.SetResult(result);
            return source.Task;
        }
    }
}