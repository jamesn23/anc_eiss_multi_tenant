﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eiss.Tenant
{
    /// <summary>
    /// Base Tenant Resolver functionality with internal Tenant List
    /// </summary>
    public abstract class ResolverBase
    {
        /// <summary>
        /// The Tenants repository(list of all the tenants/sites)
        /// </summary>
        protected List<ITenant> Tenants = new List<ITenant>();

        /// <summary>
        /// Instances a new Resolver without tenants
        /// </summary>
        protected ResolverBase()
        {
        }

        /// <summary>
        /// Instances a new resolver with a tenant list
        /// </summary>
        /// <param name="tenants">The list of tenants</param>
        protected ResolverBase(IEnumerable<ITenant> tenants)
        {
            Tenants.AddRange(tenants);
        }

        /// <summary>
        /// Adds a new tenant to resolver
        /// </summary>
        /// <param name="tenant">The tenant instance</param>
        public void Add(ITenant tenant)
        {
            Tenants.Add(tenant);
        }

        /// <summary>
        /// Retrieves the Tenants in the resolver
        /// </summary>
        /// <returns>The Tenant's list</returns>
        public List<ITenant> GetTenants()
        {
            return Tenants;
        }
    }
}